const {app, BrowserWindow} = require('electron')
 function createWindow () {   
    // 创建浏览器窗口
    win = new BrowserWindow({width: 1280, height: 720})
    var n = 20;
    console.log(n);
    // 然后加载应用的 index.html
    win.loadFile('index.html')
    win.on('closed',()=>{
    	console.log('closed')
    	win = null
    })
  }
app.on('ready', createWindow)
app.on("window-all-closed",()=>{
	console.log("window-all-closed")
	app.quit()
})
app.on('activate', () => {
    console.log('activate');
    if (win === null) {
      createWindow();
    }
  })